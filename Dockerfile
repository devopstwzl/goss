# Based on: https://github.com/kiwicom/dockerfiles/blob/master/dgoss/Dockerfile
FROM docker:latest
LABEL maintainer="Tom Sievers <t.sievers@twizzel.net>"

ENV GOSS_VERSION=v0.4.2

RUN apk add --no-cache bash &&\
    wget https://github.com/goss-org/goss/releases/download/$GOSS_VERSION/goss-linux-amd64 -O /usr/local/bin/goss-linux-amd64 && \
    wget https://github.com/goss-org/goss/releases/download/$GOSS_VERSION/dgoss -O /usr/local/bin/dgoss && \
    wget https://raw.githubusercontent.com/goss-org/goss/$GOSS_VERSION/extras/dcgoss/dcgoss -O /usr/local/bin/dcgoss

RUN printf "#!/bin/bash\nif [ -f /wait-for-lock.sh ]; then\n    echo \"Waiting for lockfile!\"\n    /wait-for-lock.sh\nfi\n/usr/local/bin/goss-linux-amd64 \"\$@\"" > /usr/local/bin/goss

RUN chmod +rx /usr/local/bin/goss /usr/local/bin/goss-linux-amd64 /usr/local/bin/dgoss /usr/local/bin/dcgoss
